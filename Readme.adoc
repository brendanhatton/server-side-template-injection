= Server Side Template Injection (SSTI)

//tag::abstract[]

SSTI happens when untrusted data is used in a server-side template engine in an unsafe
manner.  Depending on the template engine, this can
cause code execution on the server, exposure of sensitive data (e.g. secret in an environment variable) or Cross-Site Scripting.

//end::abstract[]

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

*Fork* and clone this repository.
Install `docker` and `make` on your system.

. Build the program: `make build`.
. Run it: `make run`.
. Run unit tests: `make test`. 
. Run security tests: `make securitytest`.

Note: The last test will fail. 

=== Task 1

Review the program code try to find out 
why security tests fails. 

Note: Avoid looking at tests or patch file and try to
spot the vulnerable pattern on your own.

=== Task 2

The program is vulnerable to SSTI. 
Find how to patch this vulnerability.

=== Task 3

Review `test/java/appSecuritySpec.java` and see how security tests
works. Review your patch from Task 2.
Make sure this time the security tests pass.
If you stuck, move to the next task.

=== Task 4

Check out the `patch` branch and review the program code.
Run all tests and make sure everything pass.

=== Task 5

Merge the patch branch to master.
Commit and push your changes.
Does pipeline show that you have passed the build? 

(Note: you do NOT need to send a pull request)

//end::lab[]

//tag::references[]

== References

* OWASP Application Security Verification Standard 4.0, 5.2.4
* CWE 94
* https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/Server%20Side%20Template%20Injection#java[Template Injection]
* https://github.com/DiogoMRSilva/websitesVulnerableToSSTI[Websites vulnerable to SSTI]

//end::references[]
